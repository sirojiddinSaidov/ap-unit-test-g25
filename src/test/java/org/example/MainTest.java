package org.example;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class MainTest {

    static Main main;

    @BeforeAll
    static void beforeAll() {
        main = new Main();
        System.out.println("Before all");
    }

    @AfterAll
    static void afterAll() {
        System.out.println("After all");
    }

//    @BeforeEach
//    void setKetmon() {
//        main = new Main();
//    }
//
//    @AfterEach
//    void setDestroy() {
//        main = null;
//    }

    @Order(10)
    @DisplayName(value = "First Success")
    @Test
    void fullNameSuccessTest() {
        System.out.println("First");
        String expected = "A B";
        String actual = main.fullName("A", "B");
        assertEquals(expected, actual);
    }

    @Order(2)
    @DisplayName(value = "First Throw")
    @Test
    void fullNameFNameNullThrowTest() {
        System.out.println("First th");
        assertThrows(
                NullPointerException.class,
                () -> main.fullName(null, ""));
    }

    @Order(3)
    @DisplayName(value = "Second Throw")
    @Test
    void fullNameLastNameNullThrowTest() {
        System.out.println("Second thr");
        assertThrows(
                NullPointerException.class,
                () -> main.fullName("aa", null));
    }
}