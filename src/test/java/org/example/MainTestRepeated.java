package org.example;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnJre;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.JRE;
import org.junit.jupiter.api.condition.OS;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;


@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class KetmonTest {

    static Main main;

    @BeforeAll
    static void setUp() {
        main = new Main();
    }


    @DisabledOnOs(value = OS.LINUX)
//    @DisplayName(value = "Success test")
//    @RepeatedTest(value = 10)
    @Test
    void fullNameLastNameNullThrowTest() {
        String f = UUID.randomUUID().toString();
        String l = UUID.randomUUID().toString();
        System.out.println("");
        String actual = main.fullName(f, l);
        String ex = f + " " + l;
        System.out.println(ex);
        System.out.println(actual);
        assertEquals(ex, actual);
    }

    @DisabledOnJre(value = JRE.JAVA_18)
    @Test
    void fullNameLast() {
        String f = UUID.randomUUID().toString();
        String l = UUID.randomUUID().toString();
        System.out.println("");
        String actual = main.fullName(f, l);
        String ex = f + " " + l;
        System.out.println(ex);
        System.out.println(actual);
        assertEquals(ex, actual);
    }
}