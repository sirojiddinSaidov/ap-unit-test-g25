package org.example;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.EnabledIf;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;

import java.time.Duration;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

//@Timeout(4)
@DisplayName(value = "Mani testim")
@TestMethodOrder(value = MethodOrderer.OrderAnnotation.class)
class MyParameterizedTest {

    static Main main;

    @BeforeAll
    static void beforeAll() {
        main = new Main();
        System.out.println("Before all");
    }

    @Order(10)
    @DisplayName(value = "First Success")
    @ParameterizedTest
    @ValueSource(strings = {"A*B", "B*C", "C*D", "D*E"})
    void fullNameSuccessTest(String argument) {
        String[] split = argument.split("\\*");
        String f = split[0];
        String l = split[1];
        String expected = f + " " + l;
        String actual = main.fullName(f, l);
        assertEquals(expected, actual);
    }

    @Order(100)
    @DisplayName(value = "Second Success")
    @ParameterizedTest
    @CsvSource(value = {"A,B", "C,D", "E,F", "G,H"})
    void fullNameTest(String f, String l) {
        String expected = f + " " + l;
        String actual = main.fullName(f, l);
        assertEquals(expected, actual);
    }

    @Order(100)
    @DisplayName(value = "Third Success")
    @ParameterizedTest
    @MethodSource(value = "getValue")
    void fullNameMethodSourceTest(String f, String l) {
        String expected = f + " " + l;
        String actual = main.fullName(f, l);
        assertEquals(expected, actual);
    }

    @Order(100)
    @Test
    void first() {
        boolean res = false;
        assertEquals(res, false);
        assertFalse(res, "OKa false");
    }

    @Test
    void second() {
        String ac = main.fullName("A", "B");
        String ex = "A B";
        assertAll(
                () -> assertEquals(ac, ex),
                () -> assertEquals(ex, ac)

        );
    }

    @EnabledIf(value = "myCondition")
    @Test
    void third() {
        String ac = main.fullName("A", "B");
        assertEquals(ac, "A B");
    }

    boolean myCondition(){
        return true;
    }

    @Order(1000)
    @DisplayName(value = "Fouth Success")
    @ParameterizedTest
    @CsvFileSource(resources = "/ketmon.csv", useHeadersInDisplayName = true)
    void fullNameCsvFileSourceTest(String f, String l) {
        String expected = f + " " + l;
        String actual = main.fullName(f, l);
        assertEquals(expected, actual);
    }

    @DisplayName(value = "Fouth Success")
    @Test
    void fullNameTimeoutTest() {
        String f = "A";
        String l = "B";
        String expected = f + " " + l;
        String actual = main.fullName(f, l);
//        assertTimeout(Duration.ofSeconds(2), () -> main.fullName(f, l));
//     /   assertEquals(expected, actual);
    }

    private static Stream<Arguments> getValue() {
        return Stream.of(
                Arguments.of("A", "B"),
                Arguments.of("C", "D"),
                Arguments.of("E", "F")
        );
    }

}