package org.example;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;

import java.time.DayOfWeek;

import static org.junit.jupiter.api.Assertions.*;


class MainThirdTest {


    @ParameterizedTest
    @EnumSource(mode = EnumSource.Mode.EXCLUDE, names = {"MONDAY", "FRIDAY"})
    void fullNameLastNameNullThrowTest(DayOfWeek dayOfWeek) {
        assertNotEquals(null, dayOfWeek);
    }
}